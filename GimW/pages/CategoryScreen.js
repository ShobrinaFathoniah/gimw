import React from 'react'
import axios from 'axios'
import { StyleSheet, Text, View, StatusBar, FlatList, Image, TouchableOpacity, ScrollView } from 'react-native'

export default function CategoryScreen({navigation}) {

    const [category, setCategory] = React.useState([]);

    const categories = ()=>{
        axios.get('https://www.themealdb.com/api/json/v1/1/categories.php')
        .then(res=>{
            const data1 = (res.data.categories)
            console.log('res: ', data1);
            setCategory(data1)
        })
    }

    React.useEffect(() => {
        categories()
    }, [])

    return (
        <ScrollView style={styles.container}>
            <View>
                <Text style={{fontWeight: 'bold', fontSize: 24, color: '#607C3C', marginLeft: 10}}>Category</Text>
            </View>
            <View style={{marginTop: 10}}>
                <View style={{margin: 5, alignItems: 'center'}}>
                    <FlatList
                            data={category}
                            numColumns = '2'
                            keyExtractor={(item, index) => `${item.idCategory}-${index}`}
                            renderItem={({item})=> {
                                return(
                                    <TouchableOpacity style={styles.box2} onPress={()=> {
                                        navigation.navigate("ListCategoryScreen", {
                                            strCategory: `${item.strCategory}`
                                        })
                                    }}>
                                        <View style = {styles.containerToCenter}>
                                            <Image style={styles.image2} source={{uri :`${item.strCategoryThumb}`}} />
                                            <Text style={styles.text2}>
                                                {item.strCategory}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                    />   
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding: 10,
        paddingTop: StatusBar.currentHeight,
    },
    icon:{
        width: 30,
        height: 30
    },
    containerToCenter:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    box2:{
        width: 150,
        height: 100,
        backgroundColor: '#C6D57E',
        marginLeft: 5,
        borderRadius: 5,
        marginBottom: 5
    },
    image2:{
        width: 150,
        height: 75,
        borderRadius: 5
    },
    text2:{
        fontSize: 13,
        fontWeight: 'bold',
        color: '#607C3C',
        padding: 2,
        textAlign: 'center'
    }
})
