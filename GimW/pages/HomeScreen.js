import React from 'react'
import axios from 'axios'
import { StyleSheet, Text, View, StatusBar, TextInput, FlatList, Image, TouchableOpacity, ScrollView, BackHandler, Alert } from 'react-native'

export default function HomeScreen({navigation}) {

    const [search, setSearch] = React.useState("");
    const [items, setItems] = React.useState([]);
    const [random, setRandom] = React.useState([]);

    React.useEffect(() => {
        const backAction = () => {
          Alert.alert("Hold on!", "Do you want to exit the application?", [
            {
              text: "Cancel",
              onPress: () => null,
              style: "cancel"
            },
            { text: "YES", onPress: () => BackHandler.exitApp() }
          ]);
          return true;
        };
    
        const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          backAction
        );
    
        return () => backHandler.remove();
    }, [])

    const searching = ()=>{
        axios.get(`https://www.themealdb.com/api/json/v1/1/search.php?s=${search}`)
        .then(res=>{
            const data1 = (res.data.meals)
            console.log('res: ', data1);
            setItems(data1)
        })
        setSearch("")
    }

    const randomFood = ()=>{
        axios.get('https://www.themealdb.com/api/json/v1/1/random.php')
        .then(res=>{
            const dataRandom = (res.data.meals)
            console.log('res: ', dataRandom);
            setRandom(dataRandom)
        })
    }

    React.useEffect(() => {
        randomFood()
        searching()
    }, [])

    return (
        <ScrollView style={styles.container}>
            <View style={{margin: 5}}>
                <Text style={{fontSize: 24, fontWeight: 'bold', color: '#607C3C'}}>Welcome in GimW!</Text>
            </View>
            <View>
                <Text style={{fontWeight: 'bold', fontSize: 24, color: '#607C3C', marginLeft: 10}}>Recommendation</Text>
            </View>
            <View>
                <View style={{margin: 5, alignItems: 'center'}}>
                    <FlatList
                            data={random}
                            keyExtractor={(item, index) => `${item.idMeal}-${index}`}
                            renderItem={({item})=> {
                                return(
                                    <TouchableOpacity style={styles.box2} onPress={()=> {
                                        navigation.navigate("DetailScreen", {
                                            idMeals: `${item.idMeal}`
                                        })
                                    }}>
                                        <View style = {styles.containerToCenter}>
                                            <Image style={styles.image2} source={{uri :`${item.strMealThumb}`}} />
                                            <Text style={styles.text2}>
                                                {item.strMeal}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                    />   
                </View>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 5, marginBottom: 10}}>
                <View>
                    <TextInput
                        style={styles.input}
                        onChangeText={(value)=>setSearch(value)}
                        value={search}
                        placeholder="search"
                    />
                </View>
                <TouchableOpacity onPress={searching}>
                    <Image style={styles.icon} source={require('../assets/search.png')} />
                </TouchableOpacity>
            </View>
            <View style={{marginBottom: 50, alignItems: 'center'}}>
                <FlatList
                        data={items}
                        keyExtractor={(item, index) => `${item.idMeal}-${index}`}
                        numColumns = '2'
                        renderItem={({item})=> {
                            return(
                                <TouchableOpacity style={styles.box} onPress={()=> {
                                    navigation.navigate("DetailScreen", {
                                        idMeals: `${item.idMeal}`
                                    })
                                }}>
                                    <View style = {styles.containerToCenter}>
                                        <Image style={styles.image} source={{uri :`${item.strMealThumb}`}} />
                                        <View style={styles.containerToCenter}>
                                            <Text style={styles.text}>
                                                {item.strMeal}
                                            </Text>
                                        </View>
                                        
                                    </View>
                                </TouchableOpacity>
                            )
                        }}
                />   
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding: 10,
        paddingTop: StatusBar.currentHeight,
    },
    input:{
        width: 272,
        height: 46,
        margin: 10,
        borderWidth: 4,
        padding: 10,
        borderColor: '#809C13',
        color: '#607C3C',
        borderRadius: 5
    },
    box:{
        width: 175,
        height: 125,
        backgroundColor: '#C6D57E',
        marginLeft: 5,
        borderRadius: 5,
        marginBottom: 5
    },
    text:{
        fontSize: 12,
        fontWeight: 'bold',
        color: '#607C3C',
        padding: 2,
        textAlign: 'center'
    },
    image:{
        width: 175,
        height: 75,
        borderRadius: 5
    },
    icon:{
        width: 30,
        height: 30
    },
    containerToCenter:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    box2:{
        width: 355,
        height: 202,
        backgroundColor: '#C6D57E',
        marginLeft: 5,
        borderRadius: 5,
        marginBottom: 5
    },
    image2:{
        width: 355,
        height: 150,
        borderRadius: 5
    },
    text2:{
        fontSize: 14,
        fontWeight: 'bold',
        color: '#607C3C',
        padding: 2,
        textAlign: 'center'
    }
})
