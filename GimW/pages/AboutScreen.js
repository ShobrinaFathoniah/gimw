import React from 'react'
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, Linking, StatusBar } from 'react-native'

export default function AboutScreen() {

const linkedin = 'https://www.linkedin.com/in/shobrina-fathoniah-b9273b156/';
const instagram = 'https://www.instagram.com/shobrinaf_';

    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={{alignItems: 'center', marginTop: 20}}>
                    <Image style={{width: 229, height: 126, resizeMode: 'contain'}} source={require('../assets/GimW.png')}/>
                </View>
                <View style={{alignItems: 'center'}}>
                    <View style={styles.box}>
                        <Text style={{fontSize: 18, color: '#607C3C', textAlign: 'justify', padding: 5}}>GimW is a food recipe search application with recipes from various parts of the world.</Text>
                    </View>
                </View>
                <View>
                    <View style={{justifyContent: 'flex-start', alignItems: 'flex-start', margin: 5}}>
                        <Text style={{fontSize: 24, fontWeight: 'bold', color: '#607C3C'}}>Developer</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{justifyContent: 'center', alignItems: 'center', margin: 10}}>
                            <Image style={{width: 100, height: 100, borderRadius: 50}} source={require('../assets/shobrinaf.jpeg')} />
                        </View>
                        <View style={{margin: 10, alignItems: 'center', justifyContent: 'center'}}> 
                            <Text style={{color: '#4C6926', fontSize: 20}}>Shobrina Fathoniah</Text>
                        </View>
                    </View>
                    <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', margin: 5}}>
                                <TouchableOpacity style={{margin: 10}} onPress={()=>{
                                    Linking.openURL('mailto:shobrinaf@gmail.com').catch((err) =>
                                    console.error('An error occurred', err))
                                }}>
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Image style={{width: 25, height: 25}} source={require('../assets/email.png')} />
                                        <Text style={{fontSize: 14, color: '#607C3C', marginLeft: 5}}>shobrinaf@gmail.com</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{margin: 10}} onPress={()=>{
                                    Linking.openURL('tel:+6282114006016').catch((err) =>
                                    console.error('An error occurred', err))
                                }}>
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Image style={{width: 25, height: 25}} source={require('../assets/telp.png')} />
                                        <Text style={{fontSize: 14, color: '#607C3C', marginLeft: 5}}>082114006016</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', margin: 5}}>
                                <TouchableOpacity style={{margin: 5}} onPress={()=>{
                                    Linking.openURL(linkedin).catch((err) =>
                                    console.error('An error occurred', err))
                                }}
                                >
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Image style={{width: 25, height: 25}} source={require('../assets/linkedin.png')} />
                                        <Text style={{fontSize: 14, color: '#607C3C', marginLeft: 5}}>Shobrina Fathoniah</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{margin: 5}} onPress={()=>{
                                    Linking.openURL(instagram).catch((err) =>
                                    console.error('An error occurred', err))
                                }}>
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Image style={{width: 35, height: 35}} source={require('../assets/instagram.png')} />
                                        <Text style={{fontSize: 14, color: '#607C3C', marginLeft: 5}}>shobrinaf</Text>
                                    </View>
                                    
                                </TouchableOpacity>
                            </View>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        padding: 20,
        paddingTop: StatusBar.currentHeight
    },
    box:{
        margin: 10,
        backgroundColor: '#C6D57E',
        height: 100,
        width: 296,
        borderRadius: 10,
        justifyContent: 'center',
        padding: 10
    }
})
