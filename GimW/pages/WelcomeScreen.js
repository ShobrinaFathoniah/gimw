import React from 'react'
import { StyleSheet, Image, View, Button } from 'react-native'

export default function WelcomeScreen({navigation}) {
    return (
        <View style={styles.containerUtama}>
            <View style={styles.container}>
                <View style={{width: 308, height: 74}}>
                    <Image style={{width: 308, height: 74, resizeMode: 'contain'}} source={require('../assets/text_1.png')} />
                </View>
                <View style={{margin: 10}}>
                    <View>
                        <Image style={{width: 296, height: 285}} source={require('../assets/image5.png')} />
                    </View>
                </View>
                <View style={{width: 308, height: 74}}>
                    <Image style={{width: 308, height: 74, resizeMode: 'contain'}} source={require('../assets/text1304.png')} />
                </View>
                <View style={{flexDirection: 'row'}}>
                    <View style={{width: 130, height: 50, padding:10, borderRadius: 5, fontSize: 18}}>
                        <Button 
                            onPress={() => navigation.navigate("RegisterScreen")}
                            title="REGISTRASI" color='#7FC8A9' 
                        />
                    </View>
                    <View style={{width: 79, height: 50, padding:10, borderRadius: 5, fontSize: 18}}>
                        <Button 
                            onPress={() => navigation.navigate("LoginScreen")}
                            title="LOGIN" color='#ABC32F' 
                        />
                    </View>
                </View>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    containerUtama:{
        flex:1,
        justifyContent: 'center',
        backgroundColor: '#DEEDF0'
    },
    container:{
        justifyContent: 'center',
        alignItems:'center'
    }
})
