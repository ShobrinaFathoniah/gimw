import React from 'react'
import axios from 'axios'
import { StyleSheet, Text, View, StatusBar, FlatList, Image, TouchableOpacity, ScrollView } from 'react-native'

export default function ListCategoryScreen({navigation, route}) {

    const [items, setItems] = React.useState([]);
    const {strCategory} = route.params

    const meal = ()=>{
        axios.get(`https://www.themealdb.com/api/json/v1/1/filter.php?c=${strCategory}`)
        .then(res=>{
            const data1 = (res.data.meals)
            //console.log('res: ', data1);
            setItems(data1)
        })
    }

    React.useEffect(() => {
        meal()
    }, [])

    return (
        <ScrollView style={styles.container}>
            <TouchableOpacity style={{marginTop: 10, marginBottom: 10, marginLeft: 5}} 
                onPress={()=>{ navigation.goBack()} }>
                    <Image style={{width:20, height: 20}} source={require('../assets/left-arrow.png')} />
            </TouchableOpacity>
            <View>
                <Text style={{fontWeight: 'bold', fontSize: 24, color: '#607C3C', marginLeft: 10}}>List Meals</Text>
            </View>
            <View style={{marginBottom: 50, alignItems: 'center', padding: 10, marginTop: 10}}>
                <FlatList
                        data={items}
                        keyExtractor={(item, index) => `${item.idMeal}-${index}`}
                        numColumns = '2'
                        renderItem={({item})=> {
                            return(
                                <TouchableOpacity style={styles.box} onPress={()=> {
                                    navigation.navigate("DetailScreen", {
                                        idMeals: `${item.idMeal}`
                                    })
                                }}>
                                    <View style = {styles.containerToCenter}>
                                        <Image style={styles.image} source={{uri :`${item.strMealThumb}`}} />
                                        <View style={styles.containerToCenter}>
                                            <Text style={styles.text}>
                                                {item.strMeal}
                                            </Text>
                                        </View>
                                        
                                    </View>
                                </TouchableOpacity>
                            )
                        }}
                />   
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding: 10,
        paddingTop: StatusBar.currentHeight,
    },
    box:{
        width: 175,
        height: 125,
        backgroundColor: '#C6D57E',
        marginLeft: 5,
        borderRadius: 5,
        marginBottom: 5
    },
    text:{
        fontSize: 12,
        fontWeight: 'bold',
        color: '#607C3C',
        padding: 2,
        textAlign: 'center'
    },
    image:{
        width: 175,
        height: 75,
        borderRadius: 5
    },
    icon:{
        width: 30,
        height: 30
    },
    containerToCenter:{
        alignItems: 'center',
        justifyContent: 'center'
    }
})
