import React from 'react'
import { View, Image, StyleSheet, SafeAreaView, TextInput, Button, Alert } from 'react-native'
import firebase from 'firebase'

export default function Register({navigation}) {

    const [username, onChangeText] = React.useState("");
    const [email, onChangeEmail] = React.useState("");
    const [password, onChangePassword] = React.useState("");
    const [passwordUlangi, onChangePasswordUlangi] = React.useState("");
    const [nama, onChangeNama] = React.useState("");

    const firebaseConfig = {
        apiKey: "AIzaSyAXJTsPSI3qbpMvGwlikZ7NsRXHhEoh5-s",
        authDomain: "gimw-d2281.firebaseapp.com",
        projectId: "gimw-d2281",
        storageBucket: "gimw-d2281.appspot.com",
        messagingSenderId: "84455947458",
        appId: "1:84455947458:web:12e9c61308b8c823580f5b",
        databaseUrl: "https://gimw-d2281-default-rtdb.firebaseio.com/"
        //measurementId: "G-SKBJJPVJVE"
    };

    // Initialize Firebase
    if(!firebase.apps.length){
        firebase.initializeApp(firebaseConfig);
    }

    function writeUserData(uid, username, nama, email, password) {
        firebase.database().ref('users/' + uid).set({
            uid,
            username,
            nama,
            email,
            password
        }).then((data)=>{
            console.log("Berhasil disimpan");
            console.log('data ' , data)
        }).catch((error)=>{
            console.log("Gagal disimpan");
            console.log('error ' , error)
        })
    }

    const submit=()=>{
        const data = {
            email, password, nama, username
        }

        onChangeText("")
        onChangeEmail("")
        onChangePassword("")
        onChangePasswordUlangi("")
        onChangeNama("")

        console.log(data);

        if(password === passwordUlangi){
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(()=>{
                Alert.alert("Register Berhasil")
                console.log("Register Berhasil");
                writeUserData( firebase.auth().currentUser.uid, username, nama, firebase.auth().currentUser.email, password)
                navigation.navigate("LoginScreen")
            }).catch((err)=>{
                Alert.alert("Register Gagal")
                console.log("Register Gagal");
                console.log(err);
            })
        }else{
            Alert.alert("Password tidak sama")
            console.log("password tidak sama");
        }
    }

    const back = ()=>{
        navigation.navigate("WelcomeScreen")
    }

    return (
        <View style={styles.container}>
            <View style={styles.containerToCenter}>
                <Image 
                    style={styles.logo}
                    source={require('../assets/GimW.png')}
                />
            </View>
            <SafeAreaView style={styles.containerToCenter}>
                <TextInput
                    style={styles.input}
                    onChangeText={(value)=>onChangeNama(value)}
                    value={nama}
                    placeholder="Nama Lengkap"
                />
                <TextInput
                    style={styles.input}
                    onChangeText={(value)=>onChangeText(value)}
                    value={username}
                    placeholder="Username"
                />
                <TextInput
                    style={styles.input}
                    onChangeText={(value)=>onChangeEmail(value)}
                    value={email}
                    placeholder="Email"
                    keyboardType='email-address'
                />
                <View>
                    <TextInput
                        style={styles.input}
                        onChangeText={(value)=>onChangePassword(value)}
                        value={password}
                        placeholder="Password"
                        secureTextEntry={true}
                    />
                </View>
                <View>
                    <TextInput
                        style={styles.input}
                        onChangeText={(value)=>onChangePasswordUlangi(value)}
                        value={passwordUlangi}
                        placeholder="Ulangi Password"
                        secureTextEntry={true}
                    />

                </View>
            </SafeAreaView>
            <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
                <View style={styles.button}>
                    <Button
                        onPress={back}
                        title="BACK"
                        color= '#A2CDCD'
                    />
                </View>
                <View style={styles.button}>
                    <Button
                        onPress={submit}
                        title="REGISTER"
                        color= '#7FC8A9'
                    />
                </View>
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1, 
        backgroundColor: '#DEEDF0',
        justifyContent:'center'
    },
    containerToCenter:{ 
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo:{
        height: 123,
        width: 248,
        resizeMode: 'contain',
        marginBottom: 25
    },
    iconPass:{
        height: 14
    },
    input: {
        width: 272,
        height: 46,
        margin: 12,
        borderWidth: 4,
        padding: 10,
        borderColor: '#809C13',
        color: '#607C3C',
        borderRadius: 5,
    },
    button:{
        height:50,
        width: 130,
        padding:10,
        borderRadius: 3
    }
})