import React from 'react'
import { StyleSheet, Text, View, Button, Alert, StatusBar, Image, ScrollView } from 'react-native'
import firebase from 'firebase'

export default function HomeScreen({navigation}) {

    const [user, setUser] = React.useState("");

    const firebaseConfig = {
        apiKey: "AIzaSyAXJTsPSI3qbpMvGwlikZ7NsRXHhEoh5-s",
        authDomain: "gimw-d2281.firebaseapp.com",
        projectId: "gimw-d2281",
        storageBucket: "gimw-d2281.appspot.com",
        messagingSenderId: "84455947458",
        appId: "1:84455947458:web:12e9c61308b8c823580f5b",
        databaseUrl: "https://gimw-d2281-default-rtdb.firebaseio.com/"
        //measurementId: "G-SKBJJPVJVE"
    };

    // Initialize Firebase
    if(!firebase.apps.length){
        firebase.initializeApp(firebaseConfig);
    }

    function getUserData(uid) {
        firebase.database().ref('users/' + uid).once("value", snap => {
            // console.log(snap.val())
            // console.log(snap.val().nama);
            setUser(snap.val())
        })
    }

    const dataUser = ()=> {
        firebase.auth().onAuthStateChanged(user => {
            if(user){
                getUserData(user.uid)
            }
            
        })
    }

    const logout = ()=>{
        firebase.auth().signOut()
        .then(()=>{
            //console.log("Logout Berhasil");
            Alert.alert("Logout Berhasil")
            navigation.navigate("LoginScreen")
        }).catch((err)=>{
            //console.log("Logout Gagal");
            Alert.alert("Logout Gagal")
            console.log(err);
        })
    }

    React.useEffect(() => {
        dataUser()
    })

    return (
        <ScrollView style={styles.container}>
            <View style={{margin: 10, alignItems: 'center'}}>
                <Image style={{width: 180, height: 190, borderRadius: 100}} source={require('../assets/photo-profile.jpg')} />
                <Text style={{fontSize: 24, fontWeight: 'bold', color: '#607C3C', marginTop: 10}}>{user.nama}</Text>
            </View>
            <View style={{margin: 10}}>
                <Text style={{fontSize: 18, color: '#607C3C', margin: 5}}> Username      {user.username}</Text>
                <Text style={{fontSize: 18, color: '#607C3C', margin: 5}}> Email              {user.email}</Text>
            </View>
            <View style={{alignItems: 'center'}}>
                <View style={styles.button}>
                    <Button onPress={logout} title="LOGOUT" color='#D57E7E' />  
                </View>
            </View>    
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        marginTop: 20,
        paddingTop: StatusBar.currentHeight
    },
    button: {
        width: 93, 
        height: 50, 
        borderRadius: 5,
        padding: 10, 
        margin: 10
    }
})
