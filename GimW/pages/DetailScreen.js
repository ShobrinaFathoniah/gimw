import React from 'react'
import axios from 'axios'
import { StyleSheet, Text, View, StatusBar, Linking, FlatList, Image, TouchableOpacity, ScrollView } from 'react-native'

export default function DetailScreen({navigation, route}) {

    const {idMeals} = route.params
    const [items, setItems] = React.useState([]);

    const meal = ()=>{
        axios.get(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${idMeals}`)
        .then(res=>{
            const data1 = (res.data.meals)
            console.log('res: ', data1);
            setItems(data1)
        })
    }

    React.useEffect(() => {
        meal()
    }, [])

    return (
        <ScrollView style={styles.container}>
            <TouchableOpacity style={{marginTop: 10, marginBottom: 10, marginLeft: 5}} 
            onPress={()=>{ navigation.goBack()} }>
                <Image style={{width:20, height: 20}} source={require('../assets/left-arrow.png')} />
            </TouchableOpacity>
            <FlatList 
                data={items}
                keyExtractor={(item, index) => `${item.idMeal}-${index}`}
                renderItem={({item})=> {
                    return(
                        <View>
                            <View style={{paddingBottom: 10, justifyContent: 'center', alignItems: 'center'}}>
                                <View>
                                    <Image style={{width: 500, height: 400, borderRadius: 10}} source={{uri : `${item.strMealThumb}`}} />
                                </View>
                                <View style={{margin: 10}}>
                                    <Text style={styles.subJudul}>{item.strMeal}</Text>
                                </View>
                                <View style={{marginLeft: 10, marginRight: 10}}>
                                    <Text style={{textAlign: 'center', color: '#607C3C'}}> This food is from {item.strArea} and {item.strMeal} include in the {item.strCategory} category </Text>
                                </View>
                            </View>
                            <View>
                                <View style={{marginLeft: 10}}>
                                    <Text style={styles.subJudul}>Ingredients</Text>
                                </View>
                                <View style={{marginLeft: 20, marginRight: 10}}>
                                    <Text style={styles.textBahan}>1.  {item.strIngredient1}  -  {item.strMeasure1} </Text>
                                    <Text style={styles.textBahan}>2.  {item.strIngredient2}  -  {item.strMeasure2} </Text>
                                    <Text style={styles.textBahan}>3.  {item.strIngredient3}  -  {item.strMeasure3} </Text>
                                    <Text style={styles.textBahan}>4.  {item.strIngredient4}  -  {item.strMeasure4} </Text>
                                    <Text style={styles.textBahan}>5.  {item.strIngredient5}  -  {item.strMeasure5} </Text>
                                    <Text style={styles.textBahan}>6.  {item.strIngredient6}  -  {item.strMeasure6} </Text>
                                    <Text style={styles.textBahan}>7.  {item.strIngredient7}  -  {item.strMeasure7} </Text>
                                    <Text style={styles.textBahan}>8.  {item.strIngredient8}  -  {item.strMeasure8} </Text>
                                    <Text style={styles.textBahan}>9.  {item.strIngredient9}  -  {item.strMeasure9} </Text>
                                    <Text style={styles.textBahan}>10. {item.strIngredient10}  -  {item.strMeasure10} </Text>
                                    <Text style={styles.textBahan}>11.  {item.strIngredient11}  -  {item.strMeasure11} </Text>
                                    <Text style={styles.textBahan}>12.  {item.strIngredient12}  -  {item.strMeasure12} </Text>
                                    <Text style={styles.textBahan}>13.  {item.strIngredient13}  -  {item.strMeasure13} </Text>
                                    <Text style={styles.textBahan}>14.  {item.strIngredient14}  -  {item.strMeasure14} </Text>
                                    <Text style={styles.textBahan}>15.  {item.strIngredient15}  -  {item.strMeasure15} </Text>
                                    <Text style={styles.textBahan}>16.  {item.strIngredient16}  -  {item.strMeasure16} </Text>
                                    <Text style={styles.textBahan}>17.  {item.strIngredient17}  -  {item.strMeasure17} </Text>
                                    <Text style={styles.textBahan}>18.  {item.strIngredient18}  -  {item.strMeasure18} </Text>
                                    <Text style={styles.textBahan}>19.  {item.strIngredient19}  -  {item.strMeasure19} </Text>
                                    <Text style={styles.textBahan}>20. {item.strIngredient20}  -  {item.strMeasure20} </Text>
                                </View>
                            </View>
                            <View style={{paddingLeft: 10, paddingRight: 10}}>
                                <View style={{marginTop: 10, marginBottom: 10}}>
                                    <Text style={styles.subJudul}>How To Make</Text>
                                </View>
                                <View style={{marginLeft: 15, marginRight: 15}}>
                                    <Text style={styles.textLangkah}>{item.strInstructions}</Text>
                                </View>
                            </View>
                            <View style={{justifyContent: 'center', alignItems: 'center', margin: 5,paddingTop: 20}}>
                                <Text style={{fontSize: 20, color: '#607C3C'}}>For More Information</Text>
                            </View>
                            <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', paddingBottom: 40}}>
                                <TouchableOpacity onPress={()=>{
                                    Linking.openURL(`${item.strYoutube}`).catch((err) =>
                                    console.error('An error occurred', err))
                                }}
                                >
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Image style={{width: 80, height: 80}} source={require('../assets/youtube.png')} />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{margin: 5}} onPress={()=>{
                                    Linking.openURL(`${item.strSource}`).catch((err) =>
                                    console.error('An error occurred', err))
                                }}>
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Image style={{width: 50, height: 50}} source={require('../assets/web.png')} />
                                    </View>
                                    
                                </TouchableOpacity>
                            </View>
                        </View>
                    )
                }}
            />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: StatusBar.currentHeight
    },
    textBahan:{
        fontSize: 16, 
        color: '#607C3C',
        marginTop: 5
    },
    subJudul:{
        fontSize: 24, 
        fontWeight: 'bold', 
        color: '#607C3C',
        textAlign:'center'
    },
    textLangkah:{
        textAlign: 'justify',
        fontSize: 16, 
        color: '#607C3C'
    }
})
