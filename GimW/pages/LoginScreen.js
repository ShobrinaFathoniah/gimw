import React from 'react'
import { View, Image, StyleSheet, SafeAreaView, TextInput, Button, Alert } from 'react-native'
import firebase from 'firebase'

export default function Login({navigation}) {

    const [email, onChangeText] = React.useState("");
    const [password, onChangePassword] = React.useState("");

    const firebaseConfig = {
        apiKey: "AIzaSyAXJTsPSI3qbpMvGwlikZ7NsRXHhEoh5-s",
        authDomain: "gimw-d2281.firebaseapp.com",
        projectId: "gimw-d2281",
        storageBucket: "gimw-d2281.appspot.com",
        messagingSenderId: "84455947458",
        appId: "1:84455947458:web:12e9c61308b8c823580f5b"
    };
        
    // Initialize Firebase
    if(!firebase.apps.length){
        firebase.initializeApp(firebaseConfig);
    }

    const submit = ()=>{
        const data = {
            email, password
        }
        console.log(data);

        if(email === "" && password === ""){
            Alert.alert("Maaf, email dan password anda kosong")
        }else{
            firebase.auth().signInWithEmailAndPassword(email, password)
            .then(()=> {
                console.log("SignIn Berhasil");
                Alert.alert("SignIn Berhasil")
                navigation.navigate("MainApp")
            }).catch((err)=>{
                console.log("SignIn Gagal");
                Alert.alert("Terdapat kesalahan Username atau Password Anda")
                console.log(err);
            })
            onChangeText("")
            onChangePassword("")
        }
    }

    const back = ()=>{
        navigation.navigate("WelcomeScreen")
    }

    return (
        <View style={styles.container}>
            <View style={styles.containerToCenter}>
                <Image 
                    style={styles.logo}
                    source={require('../assets/GimW.png')}
                />
            </View>
            <SafeAreaView style={styles.containerToCenter}>
                <TextInput
                    style={styles.input}
                    onChangeText={(value)=>onChangeText(value)}
                    value={email}
                    placeholder="Email"
                    textContentType= "emailAddress"
                />
                <View>
                    <TextInput
                        style={styles.input}
                        onChangeText={(value)=>onChangePassword(value)}
                        value={password}
                        placeholder="Password"
                        secureTextEntry={true}
                    />
                </View>
            </SafeAreaView>
            <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
                <View style={styles.button}>
                    <Button
                        onPress={back}
                        title="BACK"
                        color= '#A2CDCD'
                    />
                </View>
                <View style={styles.button}>
                    <Button
                        onPress={submit}
                        title="LOGIN"
                        color= '#ABC32F'
                    />
                </View>
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1, 
        backgroundColor: '#DEEDF0',
        justifyContent:'center'
    },
    containerToCenter:{ 
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo:{
        height: 138,
        width: 242,
        resizeMode: 'contain',
        marginBottom: 25
    },
    input: {
        width: 272,
        height: 46,
        margin: 12,
        borderWidth: 4,
        padding: 10,
        borderColor: '#809C13',
        color: '#607C3C',
        borderRadius: 5
    },
    button:{
        height:50,
        width: 130,
        padding:10,
        borderRadius: 3
    }
})