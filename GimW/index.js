import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Router from './router'

export default function index() {
    return (
        <Router />
    )
}

const styles = StyleSheet.create({})
