import React from 'react'
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import LoginScreen from '../pages/LoginScreen';
import AboutScreen from '../pages/AboutScreen';
import HomeScreen from '../pages/HomeScreen';
import WelcomeScreen from '../pages/WelcomeScreen';
import RegisterScreen from '../pages/RegisterScreen';
import DetailScreen from '../pages/DetailScreen';
import ProfileScreen from '../pages/ProfileScreen';
import CategoryScreen from '../pages/CategoryScreen';
import ListCategoryScreen from '../pages/ListCategoryScreen';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name = "WelcomeScreen" component={WelcomeScreen} />
                <Stack.Screen name = "RegisterScreen" component={RegisterScreen} />
                <Stack.Screen name = "DetailScreen" component={DetailScreen} />
                <Stack.Screen name = "LoginScreen" component={LoginScreen} />
                <Stack.Screen name = "ListCategoryScreen" component={ListCategoryScreen} />
                <Stack.Screen name = "HomeScreen" component={HomeScreen} />
                <Stack.Screen name = "MainApp" component={MainApp} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () =>(
        <Tab.Navigator screenOptions={{headerShown: false}}>
            <Tab.Screen 
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color, size }) => (
                      <MaterialCommunityIcons name="home" color={color} size={size} />
                    ),
                }}
                name = "HomeScreen" 
                component ={HomeScreen} />
            <Tab.Screen 
                options={{
                    tabBarLabel: 'Category',
                    tabBarIcon: ({ color, size }) => (
                      <MaterialCommunityIcons name="book" color={color} size={size} />
                    ),
                }}
                name = "CategoryScreen" 
                component ={CategoryScreen} />
            <Tab.Screen 
                options={{
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ color, size }) => (
                      <MaterialCommunityIcons name="face-profile" color={color} size={size} />
                    ),
                }}
                name = "ProfileScreen" 
                component ={ProfileScreen} />
            <Tab.Screen 
                options={{
                    tabBarLabel: 'About App',
                    tabBarIcon: ({ color, size }) => (
                      <MaterialCommunityIcons name="information" color={color} size={size} />
                    ),
                }}
                name = "AboutScreen" 
                component ={AboutScreen} />
        </Tab.Navigator>
)